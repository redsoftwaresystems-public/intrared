//! # Intrarexd DB
//!
//! `intrared_db` is a collection of utilities to obtain connections and data representations
//! of intrared db strsuctures.
//!
//! It's built on a PostgresSQL db in a hybrid relational and documental solution.
//!
//! Ideally in Intrared there are two kind of tables: `data` and `relational` table.
//!
//! A `data table` is a table with a _minimum_ set of the following columns: `id`, `version`, `data`.
//! * `id` This column is represented by a UUID type and it's the primary key.
//! * `version` This column is represented by an `INTEGER` and it's used for versioning the record. It helps preventing the case when you
//!  try to persist a recod that has been persisted by another process before.
//! * `data` This column contains the record business data and it's represented by a `JSONB` it can contains any kind of data that can serialized
//! in a json allowed type.
//!
//! A `relation table` is a table that owns just the links to other tables pk, it can be used to store relations like many-to-many.
//! This does mean that it's often made of a pair of `UUID` as foreign key on `data` tables.
//!
//! # Example
//!
//! Using an imaginary "intrared" database.
//!

//! ```rust,ignore
//! use intrared_db::*;
//!
//! fn main() {
//!     let pool = DbConnPool::new(
//!                    "localhost",
//!                    5432,
//!                    "pgactix",
//!                    Some("pgactix".to_owned()),
//!                    Some("pgactix".to_owned()),
//!                )
//!                .unwrap();
//!     let conn = pool.new_connection();
//! }
//! ```

mod db_anti_corruption;
mod dbentity;
mod dbjoin;

pub use dbentity::{DbData, DbEntity};
pub use dbjoin::{Join, JoinBuilder};
pub use uuid::Uuid;
pub use db_anti_corruption::*;

// #[cfg(test)]
// pub mod tests {
//     use super::*;
//     use core::time::Duration;

//     static DB_HOST: &str = "localhost";
//     static DB_PORT: u16 = 5433;
//     static DB_NAME: &str = "pgactix";
//     static DB_USERNAME: &str = "pgactix";
//     static DB_PASSWORD: &str = "pgactix";

//     pub fn create_test_pool() -> DbConnPool {
//         DbConnPool::new(
//             DB_HOST,
//             DB_PORT,
//             DB_NAME,
//             Some(DB_USERNAME.to_owned()),
//             Some(DB_PASSWORD.to_owned()),
//         )
//         .unwrap()
//     }

//     #[test]
//     fn connection_works() {
//         let pool = create_test_pool();
//         let _ = pool.new_connection();
//     }

//     #[test]
//     fn connection_pool_from_r2d2() {
//         let connection = format!(
//             "postgres://{}:{}@{}:{}/{}",
//             DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME
//         );
//         let manager = PostgresConnectionManager::new(connection, TlsMode::None).unwrap();
//         let pool = DbConnPool::from(
//             Builder::default()
//                 .max_size(16)
//                 .connection_timeout(Duration::from_secs(30))
//                 .build(manager)
//                 .unwrap(),
//         );
//         let _ = pool.new_connection();
//     }
// }
