# Intrared

Web application written in _Rust_ language.

Developed with

- Actix-web,
- Juniper (graphql)
- r2d2 (db connection pool)
- Postgres db
