pub mod callback;
pub mod users;

use actix_web::{web, Error as HttpError, HttpResponse};
use futures::future::Future;
use juniper::http::graphiql::graphiql_source;
use juniper::http::GraphQLRequest;
use serde_derive::*;
use crate::app_data::IntraREDAppData;
use intrared_auth_dao::{DaoDbConnection};

#[derive(Serialize, Deserialize)]
pub struct GraphQLData(GraphQLRequest);

pub struct DbConnection(DaoDbConnection);

impl From<DbConnection> for DaoDbConnection {
    fn from(ctx: DbConnection) -> DaoDbConnection {
        ctx.0
    }
}

impl From<DaoDbConnection> for DbConnection {
    fn from(ctx: DaoDbConnection) -> DbConnection {
        DbConnection(ctx)
    }
}

pub fn graphiql(ir_app_data: web::Data<IntraREDAppData>) -> HttpResponse {
    let html = graphiql_source(&format!(
        "{}://{}:{}/graphql",
        ir_app_data.config.bindings.protocol,
        ir_app_data.config.bindings.host,
        ir_app_data.config.bindings.port
    ));
    actix_web::HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(html)
}

pub fn graphql(
    st: web::Data<IntraREDAppData>,
    data: web::Json<GraphQLRequest>,
) -> impl Future<Item = HttpResponse, Error = HttpError> {
    web::block(move || {
        let res = data.execute(
            &st.schema,
            &(DbConnection::from(st.dao_db_connection.new_connection().unwrap())),
        );
        Ok::<_, serde_json::error::Error>(serde_json::to_string(&res)?)
    })
    .from_err()
    .and_then(|user| {
        Ok(HttpResponse::Ok()
            .content_type("application/json")
            .body(user))
    })
}
