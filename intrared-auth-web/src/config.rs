
use serde_derive::Deserialize;
use std::fs::File;
use std::io::prelude::*;

#[derive(Deserialize, Debug)]
pub struct Config {
    pub application_name: String,
    pub security: Security,
    pub db: Db,
    pub bindings: Bindings,
    #[serde(rename(deserialize = "redis-session"))]
    pub redis_session: RedisSession,
}

#[derive(Deserialize, Debug)]
pub struct Bindings {
    pub host: String,
    pub port: u16,
    pub protocol: String,
}

#[derive(Deserialize, Debug)]
pub struct Security {
    pub client_id: String,
    pub client_secret: String,
    pub auth_uri: String,
    pub token_uri: String,
    pub redirect_uri: String,
    pub scope: String,
}

#[derive(Deserialize, Debug)]
pub struct Db {
    pub host: String,
    pub port: u16,
    pub db_name: String,
    pub db_username: String,
    pub db_password: String,
}

#[derive(Deserialize, Debug)]
pub struct RedisSession {
    pub host: String,
    pub port: u16,
}


pub fn read_config(path: &str) -> Config {
    let mut file = File::open(path).unwrap();
    let mut config = String::new();
    file.read_to_string(&mut config).unwrap();

    toml::from_str(&config).unwrap()
}