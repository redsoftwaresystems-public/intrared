mod app_data;
mod config;
mod controllers;
mod db;
mod middleware;
mod prelude;
mod session;

use actix_redis::RedisSession;
use actix_web::{middleware as actix_middleware, web, App, HttpServer};
use config::read_config;
use ring::rand::{SecureRandom, SystemRandom};
use std::sync::Arc;

use std::env;
use std::io;

fn read_config_from_env() -> config::Config {
    let config_file = env::var("INTRARED_CONFIG").expect("INTRARED_CONFIG should be defined!");
    read_config(&config_file.to_string())
}

fn main() -> io::Result<()> {
    env::set_var("RUST_LOG", "actix_web=info");

    let config = Arc::new(read_config_from_env());

    let server_binding = format!("{}:{}", config.bindings.host, config.bindings.port);

    println!(
        "Starting intrared server: {}://{}:{}",
        config.bindings.protocol, config.bindings.host, config.bindings.port
    );
    env_logger::init();

    let app_data = app_data::IntraREDAppData::new(config.clone());

    let rnd = SystemRandom::new();
    let random_key = &mut [0; 32];
    rnd.fill(random_key).unwrap();
    let random_key = *random_key;
    let server = HttpServer::new(move || {
        App::new()
            .data(app_data.clone())
            .wrap(middleware::redirect::CheckLogin)
            .wrap(RedisSession::new(
                format!(
                    "{}:{}",
                    config.redis_session.host, config.redis_session.port
                ),
                &random_key,
            ))
            .wrap(actix_middleware::Logger::default())
            .service(
                web::resource("/callback")
                    .route(web::get().to_async(controllers::callback::callback)),
            )
            .service(web::resource("/graphql").route(web::post().to_async(controllers::graphql)))
            .service(web::resource("/graphiql").route(web::get().to(controllers::graphiql)))
    })
    .bind(server_binding)
    .unwrap();

    server.run()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::app_data;

    #[test]
    fn test_r2d2_pool() {
        let config = Arc::new(read_config_from_env());
        app_data::IntraREDAppData::new(config.clone());
    }
}
