use crate::config::*;
use crate::prelude::*;

use crate::app_data::*;
use crate::middleware::redirect::CbState;
use crate::session::{User as JwtClaims, TokenInfo, set_session_content};
use actix_web::client::Client;
use futures::future::*;

use jwt::{Token, Header};

use actix_web::{http, Error as HttpError, HttpRequest, HttpResponse};
use actix_web::web::Query;
use serde_derive::{Deserialize, Serialize};
use serde_json;
use url::Url;

type TokenResponseContent = TokenInfo;

#[derive(Serialize, Debug)]
struct AuthCodePayload {
    grant_type: String,
    code: String,
    redirect_uri: String,
    client_id: String,
    client_secret: String,
}

impl AuthCodePayload {
    fn new(security: &Security, code: &str) -> AuthCodePayload {
        AuthCodePayload {
            grant_type: String::from("authorization_code"),
            code: String::from(code),
            redirect_uri: security.redirect_uri.clone(),
            client_id: security.client_id.clone(),
            client_secret: security.client_secret.clone(),
        }
    }
}

#[derive(Deserialize)]
struct AuthRequest {
    state: String,
    code: String,
}

fn read_jwt(jwt: &str) -> Result<JwtClaims, HttpError> {
    let token = Token::<Header, JwtClaims>::parse(jwt).unwrap();
    Ok(token.claims)
}

pub fn callback(mut req: HttpRequest) -> impl Future<Item = HttpResponse, Error = HttpError> {
    let config = req.get_ir_app_data().config.clone();
    let auth_request = Query::<AuthRequest>::from_query(req.query_string()).unwrap();
    let state: String = auth_request.state.clone();
    let app_state: serde_json::Result<CbState> = serde_json::from_str(&state);
    let code = auth_request.code.clone();

    match app_state {
        Ok(ref cb_state) if code != "" /*&& cb_state.uuid == ""*/ => {
            let uri = cb_state.uri.clone();
            let auth_code_payload = AuthCodePayload::new(&config.security, &code);
            Either::A(
                Client::default()
                    .post(&config.security.token_uri)
                    .header("Content-Type", "application/json")
                    .header("Cache-Control", "no-store")
                    .header("Pragma", "no-cache")
                    .send_json(&auth_code_payload)
                    .from_err()
                    .and_then(move |mut response| {
                        response
                            .json::<TokenResponseContent>()
                            .from_err()
                            .and_then(move |content| {
                                if let Some(jwt) = content.id_token {
                                    set_session_content(req.get_session(), Some(read_jwt(&jwt).unwrap()), Some(TokenInfo{
                                        access_token: content.access_token,
                                        expires_in: content.expires_in,
                                        id_token: None,
                                        refresh_token: content.refresh_token
                                    })).unwrap();
                                }
                                HttpResponse::Found()
                                    .header(http::header::LOCATION, uri)
                                    .finish()
                                    .into_future()
                            })
                    }),
            )
        }
        _ => Either::B(
            HttpResponse::Found()
                .header(
                    http::header::LOCATION,
                    Url::parse_with_params(
                        &config.security.auth_uri,
                        &[("error", "invalid_state")],
                    )
                    .unwrap()
                    .as_str(),
                )
                .finish()
                .into_future(),
        ),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use uuid::Uuid;

    #[test]
    fn test_decode_jwt() {
        let jwt = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImRmOGQ5ZWU0MDNiY2M3MTg1YWQ1MTA0MTE5NGJkMzQzMzc0MmQ5YWEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI3Mjk1NTQ2ODE1MDctZXA1aG82NDQ3Z3JzdW1naWc5cm5yMm8xczhvNDN1N2IuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI3Mjk1NTQ2ODE1MDctZXA1aG82NDQ3Z3JzdW1naWc5cm5yMm8xczhvNDN1N2IuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTc3MTc0NjMzNTczNzU0MTQ2NTciLCJoZCI6InJlZC5zb2Z0d2FyZS5zeXN0ZW1zIiwiZW1haWwiOiJhY2hpdW1lbnRpQHJlZC5zb2Z0d2FyZS5zeXN0ZW1zIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF0X2hhc2giOiIzVmFTWk04eDNrOXJTWWZGeHNMQXd3IiwibmFtZSI6IkFuZHJlYSBDaGl1bWVudGkiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tL2EtL0FBdUU3bURuZWVkNXJGdHZUMVhDS1JxWnZzb3R2UzhYNVBQUUMtd0FaWFMwPXM5Ni1jIiwiZ2l2ZW5fbmFtZSI6IkFuZHJlYSIsImZhbWlseV9uYW1lIjoiQ2hpdW1lbnRpIiwibG9jYWxlIjoiaXQiLCJpYXQiOjE1NjY0MDg2MDIsImV4cCI6MTU2NjQxMjIwMn0.Jowz3qvqw4Ne4gGXTUVXfe68SyP1B8JAon4kDwnjKeci0F_3zcMZqB8Wdz8kPhpux4y1QVwfAAuEGfuzbV5HWmF2z8sS7aSJwcFiDiuFQWQWwpGnT8nWomoZW_gqQ5hj5yfkWWTu-Ie-QpCWJPHkfl0bh4jpasYcUuFNtT1fEalCAGF1ndhYGhT23yTEjnI4WLrZMuLQ0YV-0xgCw0c6UMUERyQEpnRpdJsrkR33XH4zvqdL7CqGUrznMvyDtBiIfjYnTkqPyretlKYrCgU198qOl_DPvZAT9795qorxvofjTp1pp0xIih96tjjxhfegqbQr5w3tt8qG5q_nzoEV3w";
        match read_jwt(jwt) {
            Err(e) => {
                panic!("Panic on {:?}", e);
            }
            Ok(user) => {
                assert_eq!(user.name, Some(String::from("Andrea Chiumenti")));
            }
        }
    }

    #[test]
    fn test_cbstate_serde() {
        let seed = Uuid::new_v4();
        let state = serde_json::to_string(&CbState {
            uuid: seed.to_hyphenated().to_string(),
            uri: String::from("/"),
        })
        .unwrap();

        let app_state: serde_json::Result<CbState> = serde_json::from_str(&state);

        match app_state {
            Err(e) => {
                panic!("Panic on {:?}", e);
            }
            Ok(app_state) => {
                assert_eq!(app_state.uuid, seed.to_hyphenated().to_string());
                assert_eq!(app_state.uri, String::from("/"));
            }
        }
    }
}
