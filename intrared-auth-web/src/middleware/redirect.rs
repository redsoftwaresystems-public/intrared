use crate::prelude::*;
use actix_service::{Service, Transform};
use actix_web::dev::{ServiceRequest, ServiceResponse};
use actix_web::{http, Error, HttpResponse};
use futures::future::{ok, Either, FutureResult};
use futures::Poll;
use serde_derive::{Deserialize, Serialize};
use serde_json;
use url::Url;
use uuid::Uuid;

use crate::session::{get_session_content, SESSION_STATE};

pub struct CheckLogin;

impl<S, B> Transform<S> for CheckLogin
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Transform = CheckLoginMiddleware<S>;
    type InitError = ();
    type Future = FutureResult<Self::Transform, Self::InitError>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(CheckLoginMiddleware { service })
    }
}

pub struct CheckLoginMiddleware<S> {
    service: S,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct CbState {
    pub uuid: String,
    pub uri: String,
}

impl<S, B> Service for CheckLoginMiddleware<S>
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Future = Either<S::Future, FutureResult<Self::Response, Self::Error>>;

    fn poll_ready(&mut self) -> Poll<(), Self::Error> {
        self.service.poll_ready()
    }

    fn call(&mut self, mut req: ServiceRequest) -> Self::Future {
        let session_content = get_session_content(req.get_session()).unwrap();

        let is_logged_in = match session_content.user {
            Some(_) => true,
            _ => false,
        };

        if is_logged_in || req.path() == "/callback" {
            Either::A(self.service.call(req))
        } else {
            let config = req.get_ir_app_data().config.clone();
            let seed = Uuid::new_v4();
            req.get_session().set(SESSION_STATE, seed).unwrap();
            let state = CbState {
                uuid: seed.to_hyphenated().to_string(),
                uri: req.uri().to_string(),
            };

            Either::B(ok(req.into_response(
                HttpResponse::Found()
                    .header(
                        http::header::LOCATION,
                        Url::parse_with_params(
                            &config.security.auth_uri,
                            &[
                                ("response_type", "code"),
                                ("client_id", &config.security.client_id),
                                ("redirect_uri", &config.security.redirect_uri),
                                ("scope", &config.security.scope),
                                ("state", &serde_json::to_string(&state).unwrap()),
                            ],
                        )
                        .unwrap()
                        .as_str(),
                    )
                    .finish()
                    .into_body(),
            )))
        }
    }
}
