use actix_session::Session;
use actix_web::Error;
use serde::{Deserialize, Serialize};

pub static SESSION_STATE: &str = "state";
pub static SESSION_USER: &str = "user";

#[derive(Serialize, Deserialize, Debug)]
pub struct SessionContent {
    pub user: Option<User>,
    pub token_info: Option<TokenInfo>,
}

#[derive(Debug, Default, Serialize, Deserialize, RustcDecodable, RustcEncodable)]
pub struct User {
    pub sub: String,
    pub hd: Option<String>,
    pub email: Option<String>,
    pub name: Option<String>,
    pub given_name: Option<String>,
    pub family_name: Option<String>,
    pub picture: Option<String>,
    pub locale: Option<String>,
    pub exp: usize,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TokenInfo {
    pub access_token: String,
    pub expires_in: u32,
    pub id_token: Option<String>,
    pub refresh_token: Option<String>,
}

pub fn set_session_content(
    session: Session,
    user: Option<User>,
    token_info: Option<TokenInfo>,
) -> Result<(), Error> {
    session.set(SESSION_USER, SessionContent { user, token_info })
}

pub fn get_session_content(session: Session) -> Result<SessionContent, Error> {
    if let Some(session_content) = session.get::<SessionContent>(SESSION_USER)? {
        Ok(session_content)
    } else {
        Ok(SessionContent {
            user: None,
            token_info: None,
        })
    }
}
