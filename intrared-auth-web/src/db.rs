
use crate::config::Config;

use intrared_auth_dao::DaoDbConnPool;
use std::sync::Arc;

pub fn create_pool(config: Arc<Config>) -> DaoDbConnPool {
    DaoDbConnPool::new(
        &config.db.host,
        config.db.port,
        &config.db.db_name,
        Some(String::from(&config.db.db_username[..])),
        Some(String::from(&config.db.db_password[..])),
    )
    .unwrap()
}