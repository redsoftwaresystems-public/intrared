use crate::config::Config;
use crate::controllers::users;
use crate::db;

use intrared_auth_dao::DaoDbConnPool;
use actix_web::dev::ServiceRequest;
use actix_web::web::{Data, HttpRequest};
use std::sync::Arc;

#[derive(Clone)]
pub struct IntraREDAppData {
    pub config: Arc<Config>,
    pub schema: Arc<users::Schema>,
    pub dao_db_connection: DaoDbConnPool,
}

impl IntraREDAppData {
    pub fn new(config: Arc<Config>) -> IntraREDAppData {
        IntraREDAppData {
            config: config.clone(),
            schema: Arc::new(users::create_schema()),
            dao_db_connection: db::create_pool(config.clone()),
        }
    }
}

pub trait AppData {
    fn get_ir_app_data(&self) -> IntraREDAppData;
}

impl AppData for ServiceRequest {
    fn get_ir_app_data(&self) -> IntraREDAppData {
        let app_data: Data<IntraREDAppData> = match self.app_data() {
            Some(app_data) => app_data,
            None => panic!("No IntraREDAppData for middleware CheckLoginMiddleware"),
        };
        app_data.get_ref().clone()
    }
}

impl AppData for HttpRequest {
    fn get_ir_app_data(&self) -> IntraREDAppData {
        let opt_data = self.get_app_data::<IntraREDAppData>();
        let app_data = match opt_data {
            Some(app_data) => app_data,
            None => panic!("No IntraREDAppData for middleware CheckLoginMiddleware"),
        };
        app_data.get_ref().clone()
    }
}
