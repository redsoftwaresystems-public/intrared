use serde_derive::*;
use std::hash::{Hash, Hasher};

use intrared_db_derive::DbData;
use serde_derive::Serialize;
use intrared_db::*;

#[pk(name = "name")]
#[schema("intrared")]
#[table("groups")]
#[derive(Serialize, Deserialize, DbData, Debug)]
pub struct Group {
    pub name: String,
    pub description: Option<String>,
}

impl PartialEq for Group {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Eq for Group {}

impl Hash for Group {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.name.hash(state);
    }
}

impl Group {
    pub fn find_by_name(
        dao_conn: &DbConnection,
        name: &str,
    ) -> Result<Option<DbEntity<Self>>, DbError> {
        DbEntity::<Group>::find_by(dao_conn, ("data->>'name'=$1", &[&name]))
    }

    pub fn find_all(
        dao_conn: &DbConnection,
        offset: i64,
        size: i64,
    ) -> Result<Vec<DbEntity<Group>>, DbError> {
        DbEntity::<Group>::find_all(dao_conn, None, Some("data->>'name'"), offset, size)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn new_group(name: &str) -> Group {
        Group {
            name: name.to_owned(),
            description: Some(String::from("description")),
        }
    }

    #[test]
    fn test_groups_tbl_exists() {
        let pool = crate::create_test_pool();
        let conn = pool.new_connection().unwrap();
        let _ = conn.query(&Group::select_part(), &[]).unwrap();
    }

    #[test]
    fn test_crud() {
        let pool = crate::create_test_pool();
        let conn = pool.new_connection().unwrap();

        const GROUP_TEST_CRUD: &str = "group_test_crud";
        let group_test_crud = new_group(GROUP_TEST_CRUD);
        let group_entity = &mut DbEntity::from_data(group_test_crud, &conn).unwrap();

        if group_entity.version != 0 {
            group_entity.delete(&conn).unwrap();
        }
        group_entity.insert(&conn).unwrap();

        let find_result = Group::find_by_name(&conn, &GROUP_TEST_CRUD);
        assert!(
            find_result.is_ok(),
            format!(
                "Failed to find group {}. {}",
                &GROUP_TEST_CRUD,
                find_result.err().unwrap().description
            )
        );
        assert!(
            find_result.unwrap().is_some(),
            format!("Group {} not found", &GROUP_TEST_CRUD)
        );

        assert!(group_entity.delete(&conn).is_ok(), "Delete Failed");

        let find_after_delete_result = Group::find_by_name(&conn, &GROUP_TEST_CRUD);

        assert!(
            find_after_delete_result.unwrap().is_none(),
            format!("Group {} not deleted", &GROUP_TEST_CRUD)
        );
    }

    #[test]
    fn test_find_all() {
        let pool = crate::create_test_pool();
        let conn = pool.new_connection().unwrap();

        const GROUP_TEST_FIND_ALL: &str = "group_test_find_all";
        let group = new_group(GROUP_TEST_FIND_ALL);

        let group_entity = &mut DbEntity::from_data(group, &conn).unwrap();

        if group_entity.version != 0 {
            group_entity.delete(&conn).unwrap();
        }

        group_entity.insert(&conn).unwrap();

        let find_result = Group::find_all(&conn, 0, 100);
        assert!(
            find_result.is_ok(),
            format!(
                "Failed to find group {}. {:?}",
                &GROUP_TEST_FIND_ALL,
                find_result.err().unwrap()
            )
        );
        let result_size = find_result.unwrap().len();
        assert!(
            result_size >= 1,
            format!("Found {} groups. Expected >= 1", result_size)
        );
    }
}
