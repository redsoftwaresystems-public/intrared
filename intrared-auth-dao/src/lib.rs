// ! Crate for OAuth2 flows.
// !
// ! This crate works with PostgreSQL v11 or grater
// !
// ! ```
// ! let pool = DaoDbConnPool::new(
// !        "localhost",
// !        5432,
// !        "dbname",
// !        Some("username".to_owned()),
// !        Some("password".to_owned()),
// !    )
// !    .unwrap();
// !
// ! let conn = pool.new_connection().unwrap();
// !
// ! let client = &Client::new(
// !            "application_name",
// !            "application_logo",
// !            "scope_1 scope_2",
// !            "homepage_url",
// !            "authorization_cb_url",
// !            "client_secret",
// !        );
// !        insert_client(&conn, client).unwrap();
// !```
// !

pub mod groups;
pub mod roles;
pub mod users;
#[cfg(test)]
use intrared_db::*;

#[cfg(test)]
pub fn create_test_pool() -> DbConnPool {
    DbConnPool::new(
        "localhost",
        5433,
        "pgactix",
        Some("pgactix".to_owned()),
        Some("pgactix".to_owned()),
    )
    .unwrap()
}

#[cfg(test)]
pub fn delete_all_records(schema: &str, table: &str, conn: &DbConnection) {
    conn.execute(&format!("DELETE FROM {}.{}", schema, table), &[])
        .unwrap();
}

mod tests {
    #[test]
    fn connection_works() {
        crate::create_test_pool();
    }
}
