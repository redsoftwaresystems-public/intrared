use serde_derive::*;
use std::hash::{Hash, Hasher};

use intrared_db_derive::DbData;
use serde_derive::Serialize;
use intrared_db::*;
use crate::groups::Group;

#[pk(app = "app", name = "name")]
#[schema("intrared")]
#[table("roles")]
#[derive(Serialize, Deserialize, DbData, Debug)]
pub struct Role {
    pub app: String,
    pub name: String,
    pub description: Option<String>,
}

impl PartialEq for Role {
    fn eq(&self, other: &Self) -> bool {
        self.app == other.app && self.name == other.name
    }
}

impl Eq for Role {}

impl Hash for Role {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.app.hash(state);
        self.name.hash(state);
    }
}

impl Role {
    pub fn find_by_app_name(
        dao_conn: &DbConnection,
        app: &str,
        name: &str,
    ) -> Result<Option<DbEntity<Self>>, DbError> {
        DbEntity::<Role>::find_by(
            dao_conn,
            ("data->>'app'=$1 AND data->>'name'=$2", &[&app, &name]),
        )
    }

    pub fn find_all_by_app(
        dao_conn: &DbConnection,
        app: &str,
    ) -> Result<Vec<DbEntity<Self>>, DbError> {
        DbEntity::<Role>::find_all(
            dao_conn,
            Some(("data->>'app'=$1", &[&app])),
            Some("data->>'name'"),
            0,
            999,
        )
    }
}

pub trait RoleTrait {
    fn get_groups(&self, dao_conn: &DbConnection) -> Result<Vec<DbEntity<Group>>, DbError>;
}

impl RoleTrait for DbEntity<Role> {
    fn get_groups(&self, dao_conn: &DbConnection) -> Result<Vec<DbEntity<Group>>, DbError> {
        let result = dao_conn
            .query(
                &format!(
                    "{} g, intrared.r_role_group rg, {}.{} r WHERE g.id = rg.id_group AND r.id = rg.id_role AND r.id = $1",
                    Group::select_part(),
                    Role::schema(),
                    Role::table_name()
                ),
                &[&self.id],
            )
            .unwrap();
        DbEntity::from_rows(&result)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const APP: &str = "app";
    fn new_role(name: &str) -> Role {
        Role {
            app: APP.to_owned(),
            name: name.to_owned(),
            description: Some(String::from("description")),
        }
    }

    #[test]
    fn test_roless_tbl_exists() {
        let pool = crate::create_test_pool();
        let conn = pool.new_connection().unwrap();
        let _ = conn.query(&Role::select_part(), &[]).unwrap();
    }

    #[test]
    fn test_crud() {
        let pool = crate::create_test_pool();
        let conn = pool.new_connection().unwrap();

        const ROLE_TEST_CRUD: &str = "role_test_crud";
        let role_test_crud = new_role(ROLE_TEST_CRUD);
        let role_entity = &mut DbEntity::from_data(role_test_crud, &conn).unwrap();

        if role_entity.version != 0 {
            role_entity.delete(&conn).unwrap();
        }
        role_entity.insert(&conn).unwrap();

        let find_result = Role::find_by_app_name(&conn, &APP, &ROLE_TEST_CRUD);
        assert!(
            find_result.is_ok(),
            format!(
                "Failed to find role {}. {}",
                &ROLE_TEST_CRUD,
                find_result.err().unwrap().description
            )
        );
        assert!(
            find_result.unwrap().is_some(),
            format!("Role {} not found", &ROLE_TEST_CRUD)
        );

        assert!(role_entity.delete(&conn).is_ok(), "Delete Failed");

        let find_after_delete_result = Role::find_by_app_name(&conn, &APP, &ROLE_TEST_CRUD);

        assert!(
            find_after_delete_result.unwrap().is_none(),
            format!("Role {} not deleted", &ROLE_TEST_CRUD)
        );
    }

    #[test]
    fn test_find_all() {
        let pool = crate::create_test_pool();
        let conn = pool.new_connection().unwrap();

        const ROLE_TEST_FIND_ALL: &str = "role_test_find_all";
        let role = new_role(ROLE_TEST_FIND_ALL);

        let role_entity = &mut DbEntity::from_data(role, &conn).unwrap();

        if role_entity.version != 0 {
            role_entity.delete(&conn).unwrap();
        }

        role_entity.insert(&conn).unwrap();

        let find_result = Role::find_all_by_app(&conn, &APP);
        assert!(
            find_result.is_ok(),
            format!(
                "Failed to find role {}. {:?}",
                &ROLE_TEST_FIND_ALL,
                find_result.err().unwrap()
            )
        );
        let result_size = find_result.unwrap().len();
        assert!(
            result_size >= 1,
            format!("Found {} roles. Expected >= 1", result_size)
        );
    }
}
