use serde_derive::*;
use std::hash::{Hash, Hasher};

use intrared_db_derive::DbData;
use serde_derive::Serialize;
use intrared_db::*;
use crate::groups::Group;

#[pk(user_name = "user_name")]
#[schema("intrared")]
#[table("users")]
#[derive(Serialize, Deserialize, DbData, Debug)]
pub struct User {
    pub user_id: Option<String>,
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub user_name: String,
}

impl PartialEq for User {
    fn eq(&self, other: &Self) -> bool {
        self.user_name == other.user_name
    }
}

impl Eq for User {}

impl Hash for User {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.user_name.hash(state);
    }
}

pub trait UserTrait {
    fn get_groups(&self, dao_conn: &DbConnection) -> Result<Vec<DbEntity<Group>>, DbError>;
}

impl UserTrait for DbEntity<User> {
    fn get_groups(&self, dao_conn: &DbConnection) -> Result<Vec<DbEntity<Group>>, DbError> {
        join_table!(
            (self, "id_user"),
            (Group, "id_group"),
            "intrared.r_user_group",
            dao_conn
        )
    }
}

// impl UserTrait for DbEntity<User> {
//     fn get_groups(&self, dao_conn: &DbConnection) -> Result<Vec<DbEntity<Group>>, DbError> {
//         let result = dao_conn
//             .query(
//                 &format!(
//                     "{} g, intrared.r_user_group ug, {}.{} u WHERE g.id = ug.id_group AND u.id = ug.id_user AND u.id = $1",
//                     Group::select_part(),
//                     User::schema(),
//                     User::table_name()
//                 ),
//                 &[&self.id],
//             )
//             .unwrap();
//         DbEntity::from_rows(&result)
//     }
// }
impl User {
    pub fn find_by_user_name(
        dao_conn: &DbConnection,
        user_name: &str,
    ) -> Result<Option<DbEntity<Self>>, DbError> {
        DbEntity::<User>::find_by(dao_conn, ("data->>'user_name'=$1", &[&user_name]))
    }

    pub fn find_by_email(
        dao_conn: &DbConnection,
        email: &str,
    ) -> Result<Option<DbEntity<Self>>, DbError> {
        DbEntity::<User>::find_by(dao_conn, ("data->>'email'=$1", &[&email]))
    }

    pub fn find_all(
        dao_conn: &DbConnection,
        offset: i64,
        size: i64,
    ) -> Result<Vec<DbEntity<User>>, DbError> {
        DbEntity::<User>::find_all(dao_conn, None, Some("data->>'user_name'"), offset, size)
    }

    pub fn find_all_by_last_name(
        dao_conn: &DbConnection,
        last_name: &str,
        offset: i64,
        size: i64,
    ) -> Result<Vec<DbEntity<User>>, DbError> {
        DbEntity::<User>::find_all(
            dao_conn,
            Some(("UPPER(data->>'last_name') LIKE UPPER($1)", &[&last_name])),
            Some("data->>'last_name'"),
            offset,
            size,
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_users_tbl_exists() {
        let pool = crate::create_test_pool();
        let conn = pool.new_connection().unwrap();
        let _ = conn.query(&User::select_part(), &[]).unwrap();
    }

    #[test]
    fn test_user_serde() {
        let user_name = String::from("user1");
        let email = String::from("user1@test.com");

        let user1 = User {
            user_name: user_name.clone(),
            email,
            user_id: Some(user_name.clone()),
            first_name: String::from("FirstName"),
            last_name: String::from("LastName"),
        };

        let ser_user = serde_json::to_value(&user1);

        assert!(ser_user.is_ok(), "Unable to serialize User")
    }

    #[test]
    fn test_id_and_version() {
        let pool = crate::create_test_pool();
        let conn = pool.new_connection().unwrap();

        let user_name = String::from("user1");
        let email = String::from("user1@test.com");

        let user1 = User {
            user_name: user_name.clone(),
            email,
            user_id: Some(user_name.clone()),
            first_name: String::from("FirstName"),
            last_name: String::from("LastName"),
        };

        let id_and_version = user1.find_table_id_and_version(&conn);
        assert!(
            id_and_version.is_ok(),
            format!(
                "User error on find_table_id_and_version: {}",
                id_and_version.err().unwrap().description
            )
        );
    }

    #[test]
    fn test_crud() {
        let pool = crate::create_test_pool();
        let conn = pool.new_connection().unwrap();

        let user_name = String::from("user1");
        let email = String::from("user1@test.com");

        let user1 = User {
            user_name: user_name.clone(),
            email,
            user_id: Some(user_name.clone()),
            first_name: String::from("FirstName"),
            last_name: String::from("LastName"),
        };
        let user_entity = &mut DbEntity::from_data(user1, &conn).unwrap();

        if user_entity.version != 0 {
            user_entity.delete(&conn).unwrap();
        }
        user_entity.insert(&conn).unwrap();

        let find_result = User::find_by_user_name(&conn, &user_name);
        assert!(
            find_result.is_ok(),
            format!(
                "Failed to find user {}. {:?}",
                &user_name,
                find_result.err().unwrap()
            )
        );
        assert!(
            find_result.unwrap().is_some(),
            format!("User {} not found", &user_name)
        );

        assert!(user_entity.delete(&conn).is_ok(), "Delete Failed");

        let find_after_delete_result = User::find_by_user_name(&conn, &user_name);

        assert!(
            find_after_delete_result.unwrap().is_none(),
            format!("User {} not deleted", &user_name)
        );
    }
}
