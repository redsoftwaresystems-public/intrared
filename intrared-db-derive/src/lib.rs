//! This crate provides Intrared's derive macros.
//!
//! ```edition2018,ignore
//! use intrared_db::*;
//!  use intrared_db_derive::DbData;
//!  use serde_derive::Serialize;
//!
//!  #[pk(field_1 = "field1", field_2 = "field2")]
//!  #[schema("schema_name")]
//!  #[table("table_name")]
//!  #[derive(Serialize, DbData)]
//!  struct TestData {
//!      field_1: String,
//!      field_2: String,
//!      some_other_filed: String,
//!  }
//!
//!  fn main() {}
//! ```
//!
extern crate proc_macro;

mod dbdata;

use proc_macro::TokenStream;

use syn;

#[proc_macro_derive(DbData, attributes(schema, table, pk))]
pub fn dbdata_macro_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();

    TokenStream::from(dbdata::impl_dbdata_macro(&ast))
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
